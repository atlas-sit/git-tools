#Default architecture: needs to be set before FROM
ARG IMAGE=cern/cc7-base:latest

FROM ${IMAGE}

ARG GIT_VERSION=2.41.0
ARG ARCH=x86_64
ARG OS=centos7

ARG GIT_LFS_VERSION=3.3.0
ARG GIT_LFS_ARCH=amd64

#Fake CVMFS directory, since git is not fully relocatable
ARG INSTDIR=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/${ARCH}/git/${GIT_VERSION}-${ARCH}-${OS}/

#Build prerequisites
RUN yum -y install wget gcc make zlib-devel curl-devel gettext git ca-certificates

#Source build of git to fake CVMFS directory
RUN mkdir -p ${INSTDIR}
RUN echo ${GIT_VERSION}
RUN wget https://mirrors.edge.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.xz
RUN tar xf git-${GIT_VERSION}.tar.xz
WORKDIR "git-${GIT_VERSION}"
RUN ./configure --prefix=${INSTDIR} #local installation
#By default, git will try to install hardlinks: we prefer softlinks
RUN make NO_INSTALL_HARDLINKS=YesPlease install

#Add ATLAS git-tools
RUN git clone https://gitlab.cern.ch/atlas-sit/git-tools.git
RUN find .
WORKDIR "./git-tools"
RUN cp git-atlas git-new-workdir git-sparse-checkout-atlas $INSTDIR/bin
RUN sed -i "s/GIT_VERSION_ARCH/$GIT_VERSION-${ARCH}-${OS}/g" setup.sh
RUN cp setup.sh ${INSTDIR}
RUN mkdir $INSTDIR/etc
RUN cp gitconfig ${INSTDIR}/etc

#Add git-lfs
RUN wget https://github.com/git-lfs/git-lfs/releases/download/v${GIT_LFS_VERSION}/git-lfs-linux-${GIT_LFS_ARCH}-v${GIT_LFS_VERSION}.tar.gz
RUN tar xzf git-lfs-linux-${GIT_LFS_ARCH}-v${GIT_LFS_VERSION}.tar.gz
WORKDIR "./git-lfs-${GIT_LFS_VERSION}"
RUN cp git-lfs $INSTDIR/bin

WORKDIR ${INSTDIR}/..
RUN tar czf git-$GIT_VERSION-$ARCH-${OS}.tgz $GIT_VERSION-$ARCH-${OS}
